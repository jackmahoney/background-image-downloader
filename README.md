#Background Image Downloader Chrome Extension

Adds a context menu item for downloading images displayed under the cursor. If multiple images are found, a selection dialog is shown. Works for Flickr, Instagram, EyeEm and any website where the "Save Image As..." does not work as expected (do to the images being render as background images or hidden in the DOM). If chrome does recognise the image as downloadable, no context menu item will be added. If you have any issues, please email jackmahoney212@gmail.com.

##Building
Define flavours in builds.json
`npm install`
`npm run build`