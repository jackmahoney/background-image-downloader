var fs = require('fs-extra');
var compress = require('zip-folder');

// read json
var builds = require('./builds.json');

function replaceTemplateVars(filePath, data) {
    // read src manifest, replace vars, and write to flavor
    var contents = fs.readFileSync(filePath).toString();
    var pattern = /{{([^}]+)}}/g;
    var match = null;

    var newContents = contents;
    while(match = pattern.exec(contents)){
        newContents = newContents.replace(match[0], JSON.stringify(data[match[1]]));
    }
    fs.writeFileSync(filePath, newContents);
}

// for each flavor, build
for(var flavor in builds){
    var properties = builds[flavor];

    var buildName = flavor + '_' + properties.version;
    var buildPath = './dist/' + buildName;
    var zipPath = buildPath + '.zip';

    // delete existing dir
    if(fs.existsSync(buildPath)){
        fs.removeSync(buildPath);
    }

    // make a dir
    fs.mkdirsSync(buildPath);

    // copy src over
    fs.copySync('./src', buildPath);

   replaceTemplateVars(buildPath + '/manifest.json', properties);
   replaceTemplateVars(buildPath + '/background.js', properties);

    // delete existing zip
    if(fs.existsSync(zipPath)){
        fs.removeSync(zipPath);
    }

    compress(buildPath, zipPath, function () {
        //fs.remove(bui)
    });


}