/**
 * Background javascript process
 */
(function () {

  chrome.contextMenus.create({ 
    id: '210590', 
    contexts: ['page', 'link'], 
    title: {{contextMenuTitle}},
    onclick: function () {
      requestLastClickedTarget();
    },
    documentUrlPatterns: {{matches}}
  });

  // handle download image requests
  chrome.runtime.onMessage.addListener(function handleMessage(request, sender, sendResponse) {
      if (request.backgroundImageDownloaderRequest){
        trackEvent("downloadRequest");
        chrome.downloads.download({ url: request.backgroundImageDownloaderRequest, method: "GET"}); 
      }
  });

  // tell app.js to attempt download
  function requestLastClickedTarget (callback) {
    chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
      trackEvent("contextMenuClick");
      chrome.tabs.sendMessage(tabs[0].id, { backgroundImageDownloader: "lastClickedTargetBackgroundRequested" });
    });
  }

  // name
  var service = analytics.getService({{name}});
  service.getConfig().addCallback(initAnalyticsConfig);
  var tracker = service.getTracker({{gua}});

  tracker.sendAppView({{name}}, "backgroundJS");

  function trackEvent(name) {
    tracker.sendEvent({{name}}, name);
  }

  function initAnalyticsConfig(){}

})();