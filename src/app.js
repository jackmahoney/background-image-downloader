/**
 * Content script
 */
(function () {

    // text
    var Strings = {
        header1: "More than one image source was found.&nbsp;",
        header2: "Select an image to download:",
        headerError: "Sorry. No image source was found.",
        errorMessage: "Perhaps this wasn't a real image? " +
        "Web developers sometimes use svg fonts and other non-image techniques to add visuals. If you think the extension should work in your case, please email the <a target='_blank' class='background-image-downloader_link mailto'>developer</a>.",
        button: "Cancel",
        emailSubject: "Background Image Downloader: Error Report",
        emailAddress: "jackmahoney212@gmail.com"
    };


	var lastClickedTarget;

    /**
     * @param styleValue
     * @returns {string}
     */
	function extractUrlFromBackgroundImageString(styleValue) {
		return styleValue.replace(/url\(["']?/, "").replace(/["']?\);?/, "");
	}

    /**
     * Do two rectangles overlap
     * @param rect1
     * @param rect2
     * @returns {boolean}
     */
	function boundingRectsOverlap(rect1, rect2) {
		return !(rect1.right < rect2.left 
			|| rect1.left > rect2.right 
			|| rect1.bottom < rect2.top 
			|| rect1.top > rect2.bottom);
	}

    /**
     * Does the node have some kind of image we can get the source for?
     * @param element
     * @returns {boolean}
     */
	function isNodeWithImageSource(element) {
		return hasValidBackgroundImageString(element) || (element.nodeName === "IMG" && element.src)
	}

    /**
     * Can't rely on element style, check computed styles
     * @param element
     * @returns {CSSStyleDeclaration.backgroundImage|*}
     */
	function getBackgroundImageString(element){
		return window.getComputedStyle(element, false).backgroundImage;
	}

    /**
     * Check for url as background image. Could be `none`
     * @param element
     * @returns {boolean}
     */
	function hasValidBackgroundImageString(element){
		return /url\(/.test(getBackgroundImageString(element));
	}

	function getNodeImageSource(element) {
		if(hasValidBackgroundImageString(element)){
			return extractUrlFromBackgroundImageString(getBackgroundImageString(element));
		} else if(element.nodeName === "IMG" && element.src) {
			return element.src;
		} else {
			return "";
		}
	}

	/**
	 * @returns array
	 */
	function getBackgroundForElement(element) {
		// first try element
		if(isNodeWithImageSource(element)){
			return [getNodeImageSource(element)];
		} else {
            // get every node in document
			var nodes = Array.prototype.slice.call(document.querySelectorAll('body *'))	;

            // save the target elements bounding box
			var elementBoundingRect = element.getBoundingClientRect();

            // filter the nodes and map to an image source
			var nodeSrcs = nodes.filter(function(node){
				// is node image-like and within bounding rect?
				return isNodeWithImageSource(node) && boundingRectsOverlap(elementBoundingRect, node.getBoundingClientRect());
			}).map(getNodeImageSource);

			return nodeSrcs;
		}
	}

    /**
     *
     * @param {array<string>} backgrounds urls
     */
    function showImageDialog(backgrounds) {
        var header = Strings.header1 + "<div class='background-image-downloader_header background-image-downloader_header-bold'>" + Strings.header2 + "</div>";

        // show dialog for container
        showDialog(header, function(dialog) {
            // create image container
            var container = document.createElement('div');
            container.className = 'background-image-downloader_images';

            // add images
            backgrounds.forEach(function(background){

                var img = document.createElement('img');
                img.className = 'background-image-downloader_image'

                // attach on clikc listener
                img.onclick = function() {
                    dialog.close();
                    downloadImage(background);
                }

                img.src = background;
                container.appendChild(img);
            });

            return container;
        })
    }

    /**
     * Build a mailto link with subject and body that represent the current browser state
     * @returns {string}
     */
    function getMailtoLink(){
        var data = "--- Diagnostics:\n\n" + JSON.stringify({
            location: window.location.href,
            userAgent: navigator.userAgent,
            lastClickedTarget: {
                nodeName: lastClickedTarget.nodeName,
                classList: lastClickedTarget.classList
            },
            height: window.innerHeight,
            width: window.innerWidth

        }) + '\n\n--- Add your message below:\n\n';
        return "mailto:" + Strings.emailAddress + "?subject=" + Strings.emailSubject + "&body=" + encodeURIComponent(data);
    }

    /**
     * Show error dialog when no images were available
     */
    function showImageErrorDialog() {
        var header = "<div class='background-image-downloader_header background-image-downloader_header-bold'>" + Strings.headerError + "</div>";
        showDialog(header, function(dialog){
            var container = document.createElement('div');
            container.className = 'background-image-downloader_contentBlock background-image-downloader_contentBlock-text'
            container.innerHTML = Strings.errorMessage;
            var mailtoLink = container.querySelector('.mailto');
            mailtoLink.href = getMailtoLink();
            return container;
        });
    }

    /**
     *
     * @param {string} header
     * @param {function} getContent
     */
	function showDialog(header, getContent) {

		// create dialog element
		var dialog = document.createElement('dialog');
		dialog.className = 'background-image-downloader_dialog';

        // create content
		var wrappers = [
		"<div class='background-image-downloader_contentBlock'>",
            "<div class='background-image-downloader_header'>" + header + "</div>",
        "</div>",
		"<div class='background-image-downloader_insertContent insertBlock'></div>",
		"<div class='background-image-downloader_contentBlock'>",
		    "<div class='background-image-downloader_controls'>",
                "<div class='background-image-downloader_button'>" + Strings.button + "</div>",
            "</div>",
        "</div>"
		].join("");

		// add content to dialog
		dialog.innerHTML = "<div class='background-image-downloader_inner'>" + wrappers + "</div>";

		// attach button listener
		dialog.querySelector('.background-image-downloader_button').onclick = function () {
			dialog.close();
		};

        dialog.querySelector('.insertBlock').appendChild(getContent(dialog));


		document.body.appendChild(dialog);
	
		dialog.showModal();
	}

    /**
     * Download an image from a given url by sending it to background.js
     * @param {string} src
     */
	function downloadImage(src) {
		chrome.runtime.sendMessage({ backgroundImageDownloaderRequest: src });;
	}

    /**
     * Main procedure
     */
    (function(){

        // when last target background requested return it
        chrome.runtime.onMessage.addListener(function handleMessage(request, sender, sendResponse) {
            if (request.backgroundImageDownloader === "lastClickedTargetBackgroundRequested"){
                var imageSources = getBackgroundForElement(lastClickedTarget);
                // if no images, show error message
                if(!imageSources || !imageSources.length){
                    showImageErrorDialog();
                    // if one image, download it
                } else if(imageSources.length === 1) {
                    downloadImage(imageSources[0]);
                    // if more than one image, let user pick
                } else {
                    showImageDialog(imageSources);
                }

            }
        });

        // attach click listener and record last mousedown target
        document.addEventListener("mousedown", function handleClick(e) {
            lastClickedTarget = e.target;
        });

    })();

})();